import React, { Component } from 'react';
import GraphComponent from './GraphComponent';
import Dygraph from 'dygraphs';

let getMeasurements = (url) => 
  getMeasurementFragment(url)
    .then(fragment => {
      if(fragment._links.next) {
        return getMeasurements(fragment._links.next.href)
          .then(nextFragment => {
            fragment._embedded.measurements.push(...nextFragment._embedded.measurements)
            return fragment
          })
      } else {
        console.log('base case.')
        return fragment
      }
    });

const getMeasurementFragment = (url) => fetch(url, {
  mode: 'cors',
}).then(res => res.json())

const tempName = 'tempName';
const humidityName = 'humName';
const pressName = 'pressName';

export default class GraphContainer extends Component {

  componentDidMount() {
    getMeasurements('https://environent-boot.herokuapp.com/measurements')
      .then(result => {
        console.log('Number of measurements:', result._embedded.measurements.length)
        return {
          temperature: result._embedded.measurements.map(elt => [new Date(elt.time), elt.temperature * 1.8 + 32]),
          humidity: result._embedded.measurements.map(elt => [new Date(elt.time), elt.humidity]),
          pressure: result._embedded.measurements.map(elt => [new Date(elt.time), elt.pressure]),
        }
      })
      .then(data => {
          new Dygraph(tempName, data.temperature, {
          labels: ['time', 'temperature'],
          showRoller: true,
          title: 'Temperature of apartment living room.',
          ylabel: 'Temperature (F)',
          legend: 'always',
          showRangeSelector: true,
          rangeSelectorPlotFillColor: 'MediumSlateBlue',
          rangeSelectorPlotFillGradientColor: 'rgba(123, 104, 238, 0)',
          colorValue: 0.9,
          fillAlpha: 0.4,
        }
      )
      return data;
    }
  ).then(data => {
      new Dygraph(humidityName, data.humidity, {
          labels: ['time', 'humidity'],
          showRoller: true,
          title: 'Humidity of apartment living room.',
          ylabel: 'Humidity',
          legend: 'always',
          showRangeSelector: true,
          rangeSelectorPlotFillColor: 'MediumSlateBlue',
          rangeSelectorPlotFillGradientColor: 'rgba(123, 104, 238, 0)',
          colorValue: 0.9,
          fillAlpha: 0.4,
        }
      )
      return data;
    }).then(data => {
      new Dygraph(pressName, data.pressure, {
          labels: ['time', 'pressure'],
          showRoller: true,
          title: 'Pressure of apartment living room.',
          ylabel: 'Pressure',
          legend: 'always',
          showRangeSelector: true,
          rangeSelectorPlotFillColor: 'MediumSlateBlue',
          rangeSelectorPlotFillGradientColor: 'rgba(123, 104, 238, 0)',
          colorValue: 0.9,
          fillAlpha: 0.4,
        }
      )
      return data;
    });
  }

  // Show statistics for data on component?
  //  Average, hi, lo?
  // Pass all info needed to create graph in as props, instead of using functional components?

  render() {
    return (
      <div>
        <GraphComponent graphName={tempName}/>
        <GraphComponent graphName={humidityName}/>
        <GraphComponent graphName={pressName}/>
      </div>
    );
  }
}
