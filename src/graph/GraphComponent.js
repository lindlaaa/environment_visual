import React from 'react';


const GraphComponent = ({graphName}) => (
  <div id={graphName} style={{width: '95%'}}></div>
)

export default GraphComponent;
